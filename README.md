## Run the following to configure the system
To create the CA on on the ansible controller(localhost):    
ansible-playbook playbooks/CA.yml

To configure the Keepalived HAProxy webservers configuration:   
ansible-playbook playbooks/BuildHAProxyWebEnv.yml

Edit your computer host file to:  
 IP_of_Keepalived  catsfood.com  
 IP_of_keepalived  bighead.com  

 or!

 Change DNS server to:   
 nameserver IP_mainDNSServer  
 nameserver IP_backupDNSServer
